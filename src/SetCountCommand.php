<?php

namespace Drupal\module_usage;

use Drupal\Core\Ajax\CommandInterface;

/**
 * Custom AJAX command class.
 */
class SetCountCommand implements CommandInterface {
  /**
   * The name of the tab [ notes|url].
   *
   * @var string
   */
  private string $tabName;
  /**
   * The machine name of the module.
   *
   * @var string
   */
  private string $machineName;
  /**
   * The number of rows.
   *
   * @var int
   */
  private int $count;

  /**
   * The constructor.
   */
  public function __construct(string $tabName, string $machineName, int $count) {
    $this->tabName = $tabName;
    $this->machineName = $machineName;
    $this->count = $count;
  }

  /**
   * Sets arguments to be passed to JS command.
   */
  public function render() {
    return [
      'command' => 'setCount',
      'tabName' => $this->tabName,
      'machineName' => $this->machineName,
      'count' => $this->count,
    ];
  }

}
