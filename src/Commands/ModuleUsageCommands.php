<?php

namespace Drupal\module_usage\Commands;

use Drupal\Component\Serialization\Json;
use Drupal\Core\File\FileSystemInterface;
use Drupal\module_usage\Services\ModuleUsageService;
use Drupal\module_usage\Services\QueryService;
use Drush\Commands\DrushCommands;

/**
 * Provide drush commands.
 *
 * @class ModuleUsageCommands
 */
class ModuleUsageCommands extends DrushCommands {
  /**
   * The module usage service.
   *
   * @var \Drupal\module_usage\Services\ModuleUsageService
   */
  private ModuleUsageService $moduleUsageService;

  /**
   * The Query service.
   *
   * @var \Drupal\module_usage\Services\QueryService
   */
  private QueryService $queryService;

  /**
   * The constructor.
   *
   * @param \Drupal\module_usage\Services\ModuleUsageService $moduleUsageService
   *   The module usage service.
   * @param \Drupal\module_usage\Services\QueryService $queryService
   *   The query service.
   */
  public function __construct(ModuleUsageService $moduleUsageService, QueryService $queryService) {
    parent::__construct();
    $this->moduleUsageService = $moduleUsageService;
    $this->queryService = $queryService;
  }

  /**
   * The export command.
   *
   * @param array $options
   *   The command line options.
   *
   * @description Export module usage data
   * @usage drush moduse:export [--file=filename.json] [--modules="mod1, mod2, ..."]
   *   Export module usage data
   * @command moduse:export
   * @aliases moduse-export
   * @option path
   * @option modules
   * @option file   *
   */
  public function export(
        array $options = [
          'path'   => 'private://moduse',
          'modules' => '',
          'file' => 'moduse.json',
        ]
  ): void {
    $modules = [];
    if (!empty($options['modules'])) {
      $modules = explode(',', str_replace(' ', '', $options['modules']));
    }

    $data = $this->moduleUsageService->exportModuleUsageData($modules);
    $directory = $options['path'];
    $file = $options['file'];
    $fullPath = sprintf('%s/%s', $directory, $file);

    if (\Drupal::service('file_system')->prepareDirectory($directory, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
      file_put_contents($fullPath, $data);
      $this->logger()->success('Export complete');
    }
    else {
      $this->logger()->error('Export failed');
    }
  }

  /**
   * The import command.
   *
   * @param array $options
   *   The command line options.
   *
   * @description Import module usage data
   * @command moduse:import
   * @aliases moduse-import
   * @option path
   *
   * @throws \Exception
   */
  public function import(
        array $options = [
          'path' => 'private://moduse',
          'file' => 'moduse.json',
        ]
  ): void {
    $directory = $options['path'];
    $file = $options['file'];
    $fullPath = sprintf('%s/%s', $directory, $file);

    if (!file_exists($fullPath)) {
      $this->logger()->error('Import failed, file not found: %file', [
        '%file' => $fullPath,
      ]);
      return;
    }

    if ($data = file_get_contents($fullPath)) {
      $data = Json::decode($data);
      if ($data) {
        $this->queryService->doImport($data);
        $this->logger()->success('Import complete');
      }
    }
  }

}
