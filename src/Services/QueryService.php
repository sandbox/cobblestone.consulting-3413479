<?php

namespace Drupal\module_usage\Services;

use Drupal\Core\Database\Connection;

/**
 * The query service, responsible for all database interaction.
 */
class QueryService {

  const MODULE_USAGE_TABLE = 'module_usage';

  const ACTIVITY_TABLE = 'module_usage_activity';
  const URL_TABLE = 'module_usage_urls';
  const NOTE_TABLE = 'module_usage_notes';

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  private Connection $database;

  /**
   * A datetime object.
   *
   * @var \Datetime
   */
  private \Datetime $datetime;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   */
  public function __construct(Connection $database) {
    $this->database = $database;
    $this->datetime = new \Datetime();
  }

  /**
   * Retrieves module record.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return \stdClass|null
   *   An object containing the fields from the record.
   */
  public function getModule(string $machineName): mixed {
    $rs = $this->database->select(self::MODULE_USAGE_TABLE, 'mu')
      ->fields('mu', [
        'machine_name',
        'name',
        'package',
        'description',
        'version',
        'status',
        'description_last_updated',
        'text_format',
        'changed',
        'created',
      ])
      ->condition('machine_name', $machineName)
      ->execute();

    $rec = $rs->fetchObject();

    if ($rec) {
      $rec->activity = $this->getActivity($machineName);
      $rec->urls = $this->getUrls($machineName);
      $rec->notes = $this->getNotes($machineName);
    }

    return $rec;
  }

  /**
   * Returns an array of module records.
   *
   * @param array $modules
   *   The module machine names to retrieve.
   *
   * @return mixed
   *   The array of records or null.
   */
  public function getModules(array $modules = []): mixed {
    $query = $this->database->select(self::MODULE_USAGE_TABLE, 'mu')
      ->fields('mu', [
        'machine_name',
        'name',
        'package',
        'description',
        'version',
        'status',
        'changed',
        'created',
      ]);

    if (!empty($modules)) {
      $query->condition('machine_name', $modules, 'IN');
    }

    $rs = $query->execute();
    return $rs->fetchAll();
  }

  /**
   * Get a list of activity records for a module.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   The activty records.
   */
  public function getActivity(string $machineName): array {
    $rs = $this->database->select(self::ACTIVITY_TABLE, 'mua')
      ->fields('mua', [
        'id',
        'machine_name',
        'activity_date',
        'current_version',
        'status',
        'event_description',
      ])
      ->condition('machine_name', $machineName)
      ->orderBy('activity_date', 'DESC')
      ->execute();

    return $rs->fetchAllAssoc('id');
  }

  /**
   * Get list of Version Changed events.
   *
   * @param string $startDate
   *   The start date for the query.
   * @param string $endDate
   *   The end date for the query.
   *
   * @return array
   *   The list of activity records.
   */
  public function getModuleVersionUpdates(string $startDate, string $endDate) : array {
    $rs = $this->database->select(self::ACTIVITY_TABLE, 'a')
      ->fields('a', [
        'machine_name',
        'activity_date',
        'current_version',
        'event_description',
        'status',
      ])

      ->fields('m', ['name'])
      ->condition('activity_date', strtotime($startDate . ' 00:00:00'), '>=')
      ->condition('activity_date', strtotime($endDate . ' 23:59:59'), '<=')
      ->condition('event_description', 'Version changed%', 'LIKE');

    $rs->join(self::MODULE_USAGE_TABLE, 'm', 'm.machine_name = a.machine_name');
    $rs->orderBy('activity_date', 'DESC');

    return $rs->execute()->fetchAll();
  }

  /**
   * Get a list of URLs for a module.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   The URL records.
   */
  public function getUrls(string $machineName): array {
    $rs = $this->database->select(self::URL_TABLE, 'muu')
      ->fields('muu', [
        'id',
        'machine_name',
        'url',
        'url_description',
        'created',
        'changed',
        'text_format',
      ])
      ->condition('machine_name', $machineName)
      ->orderBy('url', 'ASC')
      ->execute();

    return $rs->fetchAllAssoc('id');
  }

  /**
   * Retrieve all records from a table.
   *
   * @param string $table
   *   The name of the table.
   * @param array $modules
   *   Limit list to specified modules.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   *   The Query statement.
   */
  public function getAll(string $table, $modules = []) {
    $query = $this->database->select($table, 't')
      ->fields('t');

    if (!empty($modules)) {
      $query->condition('machine_name', $modules, 'IN');
    }
    return $query->execute();
  }

  /**
   * Determine if a table exists in the database.
   *
   * @param string $table
   *   The name of the table.
   *
   * @return bool
   *   Flag indicating existence of table.
   */
  public function tableExists(string $table) : bool {
    return $this->database->schema()->tableExists($table);
  }

  /**
   * Import data from JSON file.
   *
   * @param array $data
   *   The data to import.
   *
   * @throws \Exception
   */
  public function doImport($data) : void {
    $tables = [
      self::MODULE_USAGE_TABLE => 'machine_name',
      self::ACTIVITY_TABLE => 'id',
      self::URL_TABLE => 'id',
      self::NOTE_TABLE => 'id',
    ];
    $this->startTransaction();
    try {
      foreach ($tables as $table => $pk) {
        if (isset($data[$table])) {
          $this->insertRecords($table, $pk, $data[$table]);
        }
      }
    }
    catch (\Exception $e) {
      $this->rollback();
      // Re-throw the exception so it can be handled in the service/controller.
      throw $e;
    }
  }

  /**
   * Insert/update an array of records.
   *
   * @param string $table
   *   The table to save the records.
   * @param string $primaryKey
   *   The primary key for the table.
   * @param array $records
   *   The list of records.
   */
  private function insertRecords(string $table, string $primaryKey, array $records) : void {
    foreach ($records as $record) {
      $pk = $record[$primaryKey];
      unset($record[$primaryKey]);
      $this->database->merge($table)
        ->key($primaryKey, $pk)
        ->fields($record)
        ->execute();
    }
  }

  /**
   * Start a new DB transaction.
   *
   * @param string $name
   *   The name of the transaction.
   */
  private function startTransaction(string $name = '') : void {
    $this->database->startTransaction($name);
  }

  /**
   * Roll back a transaction.
   */
  private function rollback() : void {
    $this->database->rollBack();
  }

  /**
   * Get a single URL record.
   *
   * @param int $id
   *   The id of the record.
   *
   * @return mixed
   *   The URL record.
   */
  public function getUrl(int $id): mixed {
    $rs = $this->database->select(self::URL_TABLE, 'muu')
      ->fields('muu', [
        'id',
        'machine_name',
        'url',
        'url_description',
        'text_format',
        'created',
        'changed',
      ])
      ->condition('id', $id)
      ->execute();

    return $rs->fetchAssoc();
  }

  /**
   * Query to get list of modules that have documentation.
   *
   * @return array
   *   The list of modules.
   */
  public function withDocumentation(): array {
    $rs = $this->database->select(self::NOTE_TABLE, 'mun');
    $rs->addExpression('distinct(machine_name)', 'machine_name');
    $result = $rs->execute();
    $noteModules = $result->fetchAllAssoc('machine_name');

    $rs = $this->database->select(self::URL_TABLE, 'muu');
    $rs->addExpression('distinct(machine_name)', 'machine_name');
    $result = $rs->execute();
    $urlModules = $result->fetchAllAssoc('machine_name');

    $rs = $this->database->select(self::MODULE_USAGE_TABLE, 'mud')
      ->condition('description', '', '<>')
      ->isNotNull('description');
    $rs->addExpression('distinct(machine_name)', 'machine_name');
    $result = $rs->execute();
    $modules = $result->fetchAllAssoc('machine_name');

    $list = array_merge($urlModules, $noteModules, $modules);
    return array_keys($list);
  }

  /**
   * Get a single Note record.
   *
   * @param int $id
   *   The id of the record.
   *
   * @return mixed
   *   The Note record.
   */
  public function getNote(int $id): mixed {
    $rs = $this->database->select(self::NOTE_TABLE, 'mun')
      ->fields('mun', [
        'id',
        'machine_name',
        'created',
        'changed',
        'note_text',
        'note_title',
        'text_format',
      ])
      ->condition('id', $id)
      ->execute();

    return $rs->fetchAssoc();
  }

  /**
   * Get a list of Notes for a module.
   *
   * @param string $machineName
   *   The module machine name.
   *
   * @return array
   *   The Notes records.
   */
  public function getNotes(string $machineName): array {
    $rs = $this->database->select(self::NOTE_TABLE, 'mun')
      ->fields('mun', [
        'id',
        'machine_name',
        'created',
        'changed',
        'note_text',
        'note_title',
        'text_format',
      ])
      ->condition('machine_name', $machineName)
      ->orderBy('changed', 'DESC')
      ->execute();

    return $rs->fetchAllAssoc('id');
  }

  /**
   * Insert a new module.
   *
   * @param string $machineName
   *   The module machine name.
   * @param array $params
   *   The module fields.
   *
   * @return \stdClass
   *   The inserted module object.
   *
   * @throws \Exception
   */
  public function addModule(string $machineName, array $params): \stdClass {
    $this->database->insert(self::MODULE_USAGE_TABLE)
      ->fields([
        'machine_name' => $machineName,
        'name' => $params['name'] ?? $machineName,
        'package' => $params['package'] ?? '',
        'description' => $params['description'] ?? '',
        'version' => $params['version'] ?? '',
        'status' => $params['status'] ?? 0,
        'created' => $this->datetime->getTimestamp(),
        'changed' => $this->datetime->getTimestamp(),
      ])
      ->execute();

    return $this->getModule($machineName);
  }

  /**
   * Inserts a new activity record for a module.
   *
   * @param string $machineName
   *   The module machine name.
   * @param array $params
   *   The activity fields.
   *
   * @return string|int|null
   *   The ID of tne newly created record.
   *
   * @throws \Exception
   */
  public function addActivity(string $machineName, array $params): string|int|null {
    return $this->database->insert(self::ACTIVITY_TABLE)
      ->fields([
        'machine_name' => $machineName,
        'activity_date' => $params['date'],
        'current_version' => $params['version'],
        'status' => $params['status'],
        'event_description' => $params['description'],
      ])
      ->execute();
  }

  /**
   * Insert a URL record for a module.
   *
   * @param string $machineName
   *   The module machine name.
   * @param array $params
   *   The URL fields.
   *
   * @return string|int|null
   *   The ID of the newly created record.
   *
   * @throws \Exception
   */
  public function addUrl(string $machineName, array $params): string|int|null {
    return $this->database->insert(self::URL_TABLE)
      ->fields([
        'machine_name' => $machineName,
        'url' => $params['url'],
        'url_description' => $params['description'],
        'text_format' => $params['text_format'],
        'created' => $this->datetime->getTimestamp(),
        'changed' => $this->datetime->getTimestamp(),
      ])
      ->execute();
  }

  /**
   * Update a URL record for a module.
   *
   * @param int $id
   *   The ID of the URL record.
   * @param array $params
   *   The URL fields.
   *
   * @return string|int|null
   *   The ID of the newly created record.
   *
   * @throws \Exception
   */
  public function updateUrl(int $id, array $params): string|int|null {
    return $this->database->update(self::URL_TABLE)
      ->condition('id', $id)
      ->fields([
        'url' => $params['url'],
        'url_description' => $params['description'],
        'changed' => $this->datetime->getTimestamp(),
      ])
      ->execute();
  }

  /**
   * Update a Note record for a module.
   *
   * @param int $id
   *   The id of the note record.
   * @param array $params
   *   The Note fields.
   *
   * @return int|null
   *   The ID of the newly created record.
   *
   * @throws \Exception
   */
  public function updateNote(int $id, array $params): string|int|null {
    return $this->database->update(self::NOTE_TABLE)
      ->condition('id', $id)
      ->fields([
        'note_text' => $params['note_text'],
        'text_format' => $params['text_format'],
        'note_title' => $params['note_title'],
        'changed' => $this->datetime->getTimestamp(),
      ])
      ->execute();
  }

  /**
   * Update the description of a module.
   *
   * @param string $machineName
   *   The machine name.
   * @param string $description
   *   The description.
   * @param string $format
   *   The text format.
   *
   * @return int|null
   *   The result of the operation.
   */
  public function saveDescription(string $machineName, string $description, string $format) {
    return $this->database->update(self::MODULE_USAGE_TABLE)
      ->condition('machine_name', $machineName)
      ->fields([
        'description' => $description,
        'text_format' => $format,
        'description_last_updated' => $this->datetime->getTimestamp(),
      ])
      ->execute();
  }

  /**
   * Update the description of a module.
   *
   * @param string $machineName
   *   The machine name.
   * @param string $version
   *   The version number.
   *
   * @return int|null
   *   The result of the operation.
   */
  public function updateVersion(string $machineName, string $version) {
    return $this->database->update(self::MODULE_USAGE_TABLE)
      ->condition('machine_name', $machineName)
      ->fields([
        'version' => $version,
        'changed' => $this->datetime->getTimestamp(),
      ])
      ->execute();
  }

  /**
   * Deletes a URL record.
   *
   * @param int $id
   *   The id of the record to delete.
   *
   * @return bool
   *   Result of operation.
   */
  public function deleteUrl(int $id): bool {
    return $this->database->delete(self::URL_TABLE)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Deletes a Note record.
   *
   * @param int $id
   *   The id of the record to delete.
   *
   * @return bool
   *   Result of operation.
   */
  public function deleteNote(int $id): bool {
    return $this->database->delete(self::NOTE_TABLE)
      ->condition('id', $id)
      ->execute();
  }

  /**
   * Insert a new Note for a module.
   *
   * @param string $machineName
   *   The module machine name.
   * @param array $params
   *   The Note fields.
   *
   * @return string|int|null
   *   The ID of the newly created record.
   *
   * @throws \Exception
   */
  public function addNote(string $machineName, array $params): string|int|null {
    $now = new \Datetime();
    return $this->database->insert(self::NOTE_TABLE)
      ->fields([
        'machine_name' => $machineName,
        'created' => $now->getTimestamp(),
        'changed' => $now->getTimestamp(),
        'note_text' => $params['description'],
        'note_title' => $params['note_title'],
        'text_format' => $params['text_format'],
      ])
      ->execute();
  }

}
