<?php

namespace Drupal\module_usage\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Form\FormBuilder;
use Drupal\module_usage\Services\ModuleUsageService;
use Drupal\module_usage\Services\QueryService;
use Drupal\module_usage\SetCountCommand;
use Symfony\Component\HttpFoundation\Response;

/**
 * The module usage controller.
 */
class ModuleUsageController extends ControllerBase {

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilder
   */
  protected $formBuilder;

  /**
   * The module usage service.
   *
   * @var \Drupal\module_usage\Services\ModuleUsageService
   */
  private ModuleUsageService $moduleUsageService;

  /**
   * The query service.
   *
   * @var \Drupal\module_usage\Services\QueryService
   */
  private QueryService $queryService;

  /**
   * The constructor.
   *
   * @param \Drupal\Core\Form\FormBuilder $formBuilder
   *   The form builder.
   * @param \Drupal\module_usage\Services\ModuleUsageService $moduleUsageService
   *   The module usage service.
   * @param \Drupal\module_usage\Services\QueryService $queryService
   *   The module usage query service.
   */
  public function __construct(FormBuilder $formBuilder, ModuleUsageService $moduleUsageService, QueryService $queryService) {
    $this->formBuilder = $formBuilder;
    $this->moduleUsageService = $moduleUsageService;
    $this->queryService = $queryService;
  }

  /**
   * Dependency injection for the form builder.
   */
  public static function create($container) {
    return new static(
          $container->get('form_builder'),
          $container->get('module_usage.usage_service'),
      $container->get('module_usage.query')
      );
  }

  /**
   * Creates a view page for module usage documentation for a single module.
   *
   * @param string $machine_name
   *   The module machine name.
   *
   * @return array
   *   Render array for the module usage entity.
   */
  public function viewModule(string $machine_name) : array {
    $module = $this->moduleUsageService->getModuleByMachineName($machine_name);

    return [
      '#theme' => 'module_usage_view',
      '#item' => $module,
      '#description' => [
        '#type' => 'processed_text',
        '#text' => $module->description,
        '#format' => $module->text_format,
      ],
      '#activity' => $this->moduleUsageService->getModuleActivity($machine_name),
      '#notes' => $this->moduleUsageService->getModuleNotes($machine_name),
      '#urls' => $this->moduleUsageService->getModuleUrls($machine_name),
      '#attached' => [
        'library' => [
          'module_usage/module_usage',
        ],
      ],
    ];
  }

  /**
   * Creates an entity view for a module usage entity.
   *
   * @param string $machine_name
   *   The module machine name.
   *
   * @return array
   *   Render array for the module usage entity.
   */
  public function view(string $machine_name) {
    $module = $this->moduleUsageService->getModuleByMachineName($machine_name);

    return [
      '#theme' => 'module_usage',
      '#item' => $module,
      '#description' => [
        '#type' => 'processed_text',
        '#text' => $module->description,
        '#format' => $module->text_format,
      ],
      '#description_last_updated' => $module->description_last_updated ?? '',
      '#activity' => $this->moduleUsageService->getModuleActivity($machine_name),
      '#notes' => $this->moduleUsageService->getModuleNotes($machine_name),
      '#urls' => $this->moduleUsageService->getModuleUrls($machine_name),
    ];
  }

  /**
   * Retrieves a module list view.
   *
   * @return mixed
   *   The module list view.
   */
  public function list() {
    return \Drupal::service('module_usage.usage_service')->getModuleListView();
  }

  /**
   * Retrieves the Edit URL form.
   *
   * @param string $machine_name
   *   The  module machine name.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response
   */
  public function getUrlForm(string $machine_name) : AjaxResponse {
    $response = new AjaxResponse();

    // Render the form.
    $form = $this->formBuilder->getForm('Drupal\module_usage\Form\AddEditURLForm', $machine_name);

    $modalOptions = [
      'width' => '50%',
    ];
    $response->addCommand(new OpenModalDialogCommand($this->t('Add URL'), $form, $modalOptions));
    return $response;
  }

  /**
   * Retrieves the Edit URL form.
   *
   * @param string $machine_name
   *   The module machine name.
   * @param int $paragraph_id
   *   The paragraph ID.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getUrlEditForm(string $machine_name, int $paragraph_id) : AjaxResponse {
    $response = new AjaxResponse();

    // Render the form.
    $form = $this->formBuilder->getForm('Drupal\module_usage\Form\AddEditURLForm', $machine_name, $paragraph_id);
    $modalOptions = [
      'width' => '50%',
    ];
    $response->addCommand(new OpenModalDialogCommand($this->t('Edit URL'), $form, $modalOptions));

    return $response;
  }

  /**
   * Retrieves the Description Edit form.
   *
   * @param string $machine_name
   *   The module machine name.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getDescriptionEditForm(string $machine_name) : AjaxResponse {
    $response = new AjaxResponse();

    // Render the form.
    $form = $this->formBuilder->getForm('Drupal\module_usage\Form\EditDescriptionForm', $machine_name);

    $modalOptions = [
      'width' => '50%',
    ];

    $response->addCommand(new OpenModalDialogCommand($this->t('Edit Description'), $form, $modalOptions));
    return $response;
  }

  /**
   * Retrieves the Note add form.
   *
   * @param string $machine_name
   *   The module machine name.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getNoteForm(string $machine_name) : AjaxResponse {
    $response = new AjaxResponse();

    // Get the form.
    $form = $this->formBuilder->getForm('Drupal\module_usage\Form\AddEditNoteForm', $machine_name);
    $modalOptions = [
      'width' => '50%',
    ];
    $response->addCommand(new OpenModalDialogCommand($this->t('Add Note'), $form, $modalOptions));

    return $response;
  }

  /**
   * Retrieves the Note edit form.
   *
   * @param string $machine_name
   *   The machine name for the module.
   * @param int $paragraph_id
   *   The paragraph ID.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   */
  public function getNoteEditForm(string $machine_name, int $paragraph_id) : AjaxResponse {
    $response = new AjaxResponse();

    // Render the form.
    $form = $this->formBuilder->getForm('Drupal\module_usage\Form\AddEditNoteForm', $machine_name, $paragraph_id);
    $modalOptions = [
      'width' => '50%',
    ];
    $response->addCommand(new OpenModalDialogCommand($this->t('Edit Note'), $form, $modalOptions));
    return $response;
  }

  /**
   * Deletes a URL from a module usage entity.
   *
   * @param string $machine_name
   *   The machine name of the module.
   * @param int $id
   *   The ID of the URL.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Exception
   */
  public function deleteUrl(string $machine_name, int $id) : AjaxResponse {
    $response = new AjaxResponse();
    $this->moduleUsageService->deleteUrl($id);
    $content = $this->moduleUsageService->renderUrls($machine_name);
    $urls = $this->queryService->getUrls($machine_name);

    if (empty($content)) {
      $content = '<table id="url-table-' . $machine_name . '"><tr><th>URL</th><th>Notes</th></tr></table>';
    }
    $response->addCommand(new ReplaceCommand('#url-container-' . $machine_name, $content));
    $response->addCommand(new SetCountCommand('url', $machine_name, count($urls)));
    return $response;
  }

  /**
   * Delete a note from a module usage entity.
   *
   * @param string $machine_name
   *   The machine name of the module.
   * @param int $note_id
   *   The Note ID.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Exception
   */
  public function deleteNote(string $machine_name, int $note_id) : AjaxResponse {
    $response = new AjaxResponse();
    $status = $this->moduleUsageService->deleteNote($note_id);
    $content = $this->moduleUsageService->renderNotes($machine_name);
    $notes = $this->queryService->getNotes($machine_name);

    $response->addCommand(new ReplaceCommand('#note-container-' . $machine_name, $content));
    $response->addCommand(new SetCountCommand('notes', $machine_name, count($notes)));
    return $response;
  }

  /**
   * Retrieves a render array for a module.
   *
   * @param string $machine_name
   *   The machine name of the module.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The ajax response.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getModule(string $machine_name) {
    $response = new AjaxResponse();
    $content = $this->moduleUsageService->renderModule($machine_name);
    $response->addCommand(new ReplaceCommand('#extension-' . $machine_name, $content));
    return $response;
  }

  /**
   * Export module usage data.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The response.
   */
  public function export() : Response {
    // Call your export function here to ensure the file is created/updated.
    $data = $this->moduleUsageService->exportModuleUsageData();

    // Create a BinaryFileResponse object for the file.
    $response = new Response($data);
    $response->headers->set('Content-Disposition', 'attachment; filename="module_usage.json"');
    $response->headers->set('Content-Length', strlen($data));

    return $response;
  }

  /**
   * Report page.
   *
   * @return array
   *   The render array for the report.
   */
  public function report() : array {
    $hasDoc = $this->queryService->withDocumentation();
    $data = $this->moduleUsageService->exportModuleUsageData($hasDoc, FALSE);

    $data = $this->processReportData($data);

    return [
      '#theme' => 'module_usage_report',
      '#module_data' => $data,
      '#attached' => [
        'library' => 'module_usage/report',
      ],
    ];
  }

  /**
   * Restructure the data array from export.
   *
   * @param array $data
   *   The data array to restructure.
   *
   * @return array
   *   The restructured array.
   */
  private function processReportData($data) {
    $return = [];
    foreach ($data as $table => $info) {
      foreach ($info as $idx => $rec) {
        $return[$rec['machine_name']][$table][] = $this->processTextField($table, $rec);
      }
    }
    return $return;
  }

  /**
   * Create a render array for a long text field.
   *
   * @param string $table
   *   The table containing the field.
   * @param array $rec
   *   The record containing the field.
   *
   * @return mixed
   *   The render array.
   */
  private function processTextField($table, $rec) {
    switch ($table) {
      case 'module_usage':
        $text_field = 'description';
        break;

      case 'module_usage_notes':
        $text_field = 'note_text';
        break;

      case 'module_usage_urls':
        $text_field = 'url_description';
        break;

      default:
        return $rec;
    }

    $rec[$text_field] = [
      '#type' => 'processed_text',
      '#text' => $rec[$text_field],
      '#format' => $rec['text_format'],
    ];

    return $rec;
  }

}
