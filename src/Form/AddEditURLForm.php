<?php

namespace Drupal\module_usage\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\module_usage\SetCountCommand;

/**
 * URL Add/Edit form.
 */
class AddEditURLForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_edit_url_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $buildInfo = $form_state->getBuildInfo();
    $machine_name = $buildInfo['args'][0];
    $urlID = $buildInfo['args'][1] ?? 0;

    $urlObj = NULL;
    if ($urlID) {
      $urlObj = \Drupal::service('module_usage.usage_service')->getUrl($urlID);
    }

    $formats = filter_formats();
    $default_format = ($formats) ? key($formats) : '';
    $text_format = ($urlObj) ? $urlObj['text_format'] : $default_format;

    $url = ($urlObj) ? $urlObj['url'] : '';
    $notes = ($urlObj) ? $urlObj['url_description'] : '';

    $form['form-messages'] = [
      '#markup' => '<div id="form-messages"></div>',
    ];

    $form['url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('URL'),
      '#required' => TRUE,
      '#default_value' => $url,
      '#description' => $this->t('This must be an internal path such as %add-node. You can also start typing the title of a piece of content to select it.', [
        '%add-node' => '/node/add',
      ]),
    ];

    $form['notes'] = [
      '#type' => 'text_format',
      '#format' => $text_format,
      '#title' => $this->t('Notes'),
      '#default_value' => $notes,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::submitAjaxForm',
        'wrapper' => 'url-table-' . $machine_name,
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'core/jquery.form';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';
    $form['#token'] = FALSE;

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // @todo Change the autogenerated stub
    parent::validateForm($form, $form_state);
    $link = $form_state->getValue('url');
    // Check if the link is internal.
    if (!empty($link) && UrlHelper::isExternal($link)) {
      $form_state->setErrorByName('url', $this->t('Only internal links are allowed.'));
    }

    /** @var \Drupal\Core\Path\PathValidatorInterface $pathValidator */
    $pathValidator = \Drupal::service('path.validator');
    $url = $pathValidator->getUrlIfValid($link);
    if (empty($url)) {
      $form_state->setErrorByName('url', $this->t('Url is not valid or access is denied'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle form submission for non-AJAX submission.
  }

  /**
   * AJAX form submission handler.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The Ajax Response.
   */
  public function submitAjaxForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $url = $form_state->getValue('url');
    $notes = $form_state->getValue('notes');
    $buildInfo = $form_state->getBuildInfo();
    $machine_name = $buildInfo['args'][0] ?? '';
    $pid = $buildInfo['args'][1] ?? '';

    $errors = $form_state->getErrors();
    if (!empty($errors)) {
      $messages = [
        '#type' => 'status_messages',
        '#status_messages' => ['error'],
      ];

      $response->addCommand(new HtmlCommand('#form-messages', $messages));
      return $response;
    }

    /**
      * @var \Drupal\module_usage\Services\ModuleUsageService $service
      */
    $service = \Drupal::service('module_usage.usage_service');
    $entity = NULL;

    if ($pid) {
      $service->editUrl($pid, $url, $notes);
      $entity = $service->getModuleByMachineName($machine_name);
    }
    elseif ($machine_name) {
      $entity = $service->addUrl($machine_name, $url, $notes['value'], $notes['format']);
    }

    if ($entity) {

      /**
       * @var \Drupal\module_usage\Services\QueryService $queryService
       */
      $queryService = \Drupal::service('module_usage.query');
      $urls = $queryService->getUrls($machine_name);

      $content = $service->renderUrls($machine_name);

      $response->addCommand(new ReplaceCommand('#url-container-' . $machine_name, $content));
      $response->addCommand(new CloseModalDialogCommand());
      $response->addCommand(new InvokeCommand('#url-container-' . $machine_name, 'focus', []));
      $response->addCommand(new SetCountCommand('url', $machine_name, count($urls)));

      return $response;
    }

    $response->addCommand(new MessageCommand('Unable to update module documentation'));
    return $response;
  }

}
