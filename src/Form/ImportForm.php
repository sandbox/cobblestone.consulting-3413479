<?php

namespace Drupal\module_usage\Form;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;

/**
 * Provides a form for uploading a file.
 */
class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['file_upload'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Upload file'),
      '#description' => $this->t('Upload a file to import.'),
    // Adjust the upload location.
      '#upload_location' => 'private://import_files/',
      '#upload_validators' => [
    // Specify allowed file extensions.
        'file_validate_extensions' => ['json txt'],
      ],
      '#required' => TRUE,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Import'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Custom validation for the file upload, if needed.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $file_id = $form_state->getValue(['file_upload', 0]);
    if ($file_id) {
      // Load the file object based on file ID.
      $file = File::load($file_id);

      // Move the file to a temporary location to ensure it's accessible.
      // Note: Drupal's temporary files are periodically cleaned up.
      $file->setTemporary();
      $file->save();

      // Retrieve the real path of the file.
      $file_path = $file->getFileUri();
      $real_path = \Drupal::service('file_system')->realpath($file_path);

      // Open the file and read the contents.
      if ($real_path && file_exists($real_path)) {
        $contents = file_get_contents($real_path);
        if ($contents) {
          $data = Json::decode($contents);
          if ($data) {
            try {
              \Drupal::service('module_usage.query')->doImport($data);
            }
            catch (\Exception $e) {
              $this->messenger()->addError($this->t('There was an error importing the data: %err',
                [
                  '%err' => $e->getMessage(),
                ]
              ));
            }
          }
          else {
            $this->messenger()->addError($this->t('Unable to import data - Invalid format (must be JSON).'));
          }
        }
        else {
          $this->messenger()->addError($this->t('Unable to read contents or file is empty.'));
        }
      }
      else {
        // File does not exist.
        $this->messenger()->addError($this->t('File does not exist.'));
      }

      // Delete the file after processing.
      $file->delete();

      // Display a success message.
      $this->messenger()->addMessage($this->t('The file contents have been processed successfully.'));
    }
  }

}
