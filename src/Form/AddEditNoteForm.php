<?php

namespace Drupal\module_usage\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\MessageCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\module_usage\SetCountCommand;

/**
 * The Add/Edit Note Form.
 */
class AddEditNoteForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'add_edit_note_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $buildInfo = $form_state->getBuildInfo();
    $machine_name = $buildInfo['args'][0];
    $noteId = $buildInfo['args'][1] ?? 0;

    $noteObj = NULL;
    if ($noteId) {
      $noteObj = \Drupal::service('module_usage.usage_service')->getNote($noteId);
    }

    $formats = filter_formats();
    $default_format = ($formats) ? key($formats) : '';
    $text_format = ($noteObj) ? $noteObj['text_format'] : $default_format;

    $notes = ($noteId) ? $noteObj['note_text'] : '';
    $note_title = ($noteId) ? $noteObj['note_title'] : '';

    $form['note_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $note_title,
    ];

    $form['notes'] = [
      '#type' => 'text_format',
      '#format' => $text_format,
      '#title' => $this->t('Note'),
      '#default_value' => $notes,
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#ajax' => [
        'callback' => '::submitAjaxForm',
        'wrapper' => 'note-table-' . $machine_name,
      ],
    ];

    $form['#attached']['library'][] = 'core/drupal.ajax';
    $form['#attached']['library'][] = 'core/jquery.form';
    $form['#attached']['library'][] = 'core/drupal.dialog.ajax';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle form submission for non-AJAX submission.
  }

  /**
   * AJAX form submission handler.
   *
   * @param array $form
   *   The Form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The FormState object.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   The AJAX response.
   */
  public function submitAjaxForm(array &$form, FormStateInterface $form_state) {
    $response = new AjaxResponse();
    $notes = $form_state->getValue('notes');
    $note_title = $form_state->getValue('note_title');
    $buildInfo = $form_state->getBuildInfo();
    $machine_name = $buildInfo['args'][0] ?? '';
    $pid = $buildInfo['args'][1] ?? '';

    /**
     * @var \Drupal\module_usage\Services\ModuleUsageService $service
     */
    $service = \Drupal::service('module_usage.usage_service');
    $entity = NULL;

    if ($pid) {
      $service->editNote($pid, $notes, $note_title);
      $entity = $service->getModuleByMachineName($machine_name);
    }
    elseif ($machine_name) {
      $entity = $service->addNote($machine_name, $notes, $note_title);
    }

    if ($entity) {

      /**
       * @var \Drupal\module_usage\Services\QueryService $queryService
       */
      $queryService = \Drupal::service('module_usage.query');
      $notes = $queryService->getNotes($machine_name);
      $content = $service->renderNotes($machine_name);

      $response->addCommand(new ReplaceCommand('#note-container-' . $machine_name, $content));
      $response->addCommand(new CloseModalDialogCommand());
      $response->addCommand(new InvokeCommand('#note-container-' . $machine_name, 'focus', []));
      $response->addCommand(new SetCountCommand('notes', $machine_name, count($notes)));
      return $response;
    }

    $response->addCommand(new MessageCommand('Unable to update module documentation'));
    return $response;
  }

}
