<?php

use Drupal\paragraphs\Entity\ParagraphsType;

const VERSION = '1.0.0@alpha1';

/**
 * Implements hook_install().
 */
function module_usage_install($is_syncing) : void {
  $moduleInfo = Drupal::service('extension.list.module')->reset()->getList();

  /** @var \Drupal\module_usage\Services\ModuleUsageService $mudService */
  $mudService = Drupal::service('module_usage.usage_service');
  foreach ($moduleInfo as $module => $info) {
    try {
      $mudService->addModule($module, $info);
    }
    catch (Exception $exception) {
      Drupal::logger('module_usage')->error('Unable to add module %m during install: %msg', [
        '%m' => $module,
        '%msg' => $exception->getMessage(),
      ]);
    }
  }
}

/**
 * Implements hook_schema().
 */
function module_usage_schema() : array {
  $schema = [];

  $schema['module_usage'] = [
    'fields' => [
      'machine_name' => [
        'description' => 'Primary Key: Unique ID.',
        'type' => 'varchar', // Auto-incrementing integer.
        'length' => 255,
        'not null' => TRUE,
      ],
      'name' => [
        'description' => 'Module Name.',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
      'package' => [
        'description' => 'Package',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
      'description' => [
        'description' => 'Module usage description.',
        'type' => 'text', // Integer.
        'not null' => FALSE,
      ],
      'text_format' => [
        'description' => 'Format',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => FALSE,
      ],
      'description_last_updated' => [
        'description' => 'Description last updated date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
      'version' => [
        'description' => 'Module Version.',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'Boolean indicating status.',
        'type' => 'int', // Integer for boolean values (0 or 1).
        'not null' => TRUE,
        'default' => 1,
      ],
      'created' => [
        'description' => 'Creation date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
      'changed' => [
        'description' => 'Modified date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
    ],
    'primary key' => ['machine_name'],
  ];

  $schema['module_usage_activity'] = [
    'fields' => [
      'id' => [
        'description' => 'Primary Key: Unique ID.',
        'type' => 'serial', // Auto-incrementing integer.
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'machine_name' => [
        'description' => 'Reference to module_usage table',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
      ],
      'activity_date' => [
        'description' => 'Date of activity',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'current_version' => [
        'description' => 'Module Version.',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
      'status' => [
        'description' => 'Boolean indicating status.',
        'type' => 'int', // Integer for boolean values (0 or 1).
        'not null' => TRUE,
        'default' => 1,
      ],
      'event_description' => [
        'description' => 'Description of event.',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
    ],
    'primary key' => ['id'],
  ];

  $schema['module_usage_urls'] = [
    'fields' => [
      'id' => [
        'description' => 'Primary Key: Unique ID.',
        'type' => 'serial', // Auto-incrementing integer.
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'machine_name' => [
        'description' => 'Reference to module_usage table',
        'type' => 'varchar', // Auto-incrementing integer.
        'length' => 255,
        'not null' => TRUE,
      ],
      'url' => [
        'description' => 'Internal URL.',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => TRUE,
      ],
      'url_description' => [
        'description' => 'URL description.',
        'type' => 'text', // Variable length string.
      ],
      'text_format' => [
        'description' => 'Format',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => FALSE,
      ],
      'created' => [
        'description' => 'Creation date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
      'changed' => [
        'description' => 'Modified date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
  ];

  $schema['module_usage_notes'] = [
    'fields' => [
      'id' => [
        'description' => 'Primary Key: Unique ID.',
        'type' => 'serial', // Auto-incrementing integer.
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'machine_name' => [
        'description' => 'Reference to module_usage table',
        'type' => 'varchar', // Auto-incrementing integer.
        'length' => 255,
        'not null' => TRUE,
      ],
      'note_title' => [
        'description' => 'Title',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => FALSE,
      ],
      'note_text' => [
        'description' => 'The note',
        'type' => 'text', // Variable length string.
      ],
      'text_format' => [
        'description' => 'Format',
        'type' => 'varchar', // Variable length string.
        'length' => 255,
        'not null' => FALSE,
      ],
      'created' => [
        'description' => 'Creation date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
      'changed' => [
        'description' => 'Modified date.',
        'type' => 'int', // Integer.
        'not null' => FALSE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}

/**
 * Update legacy 'Version Changed' events.
 */
function module_usage_update_10001(&$sandbox) {
  $connection = \Drupal::database();
  $cnt = $connection->update(\Drupal\module_usage\Services\QueryService::ACTIVITY_TABLE)
    ->fields([
      'event_description' => 'Version changed from unknown',
    ])
    ->condition('event_description', 'Version changed')
    ->execute();

  \Drupal::messenger()->addMessage(t('Updated @num records', ['@num' => $cnt]));
}
